#!/bin/bash

# Installs node.js + dependencies for both Ubuntu and OS X

#
# See https://git.colaj86.com/coolaj86/node-installer.sh
#

# curl -fsSL https://example.com/setup.bash | bash
# wget -nv https://example.com/setup.bash -O - | bash

set -e
set -u

BASE_URL="https://git.coolaj86.com/coolaj86/node-installer.sh/raw/master"

#######################
# Download installers #
#######################

INSTALL_FILE_REMOTE="install.sh"
INSTALL_FILE="node-installer.sh"
if [ ! -e "/tmp/${INSTALL_FILE}" ]
then
  if [ -n "$(which curl)" ]; then
    curl --silent -L "${BASE_URL}/${INSTALL_FILE_REMOTE}" \
      -o "/tmp/${INSTALL_FILE}" || echo 'error setup script: '"${BASE_URL}/${INSTALL_FILE_REMOTE}"
  elif [ -n "$(which wget)" ]; then
    wget --quiet "${BASE_URL}/${INSTALL_FILE_REMOTE}" \
      -O "/tmp/${INSTALL_FILE}" || echo 'error setup script: '"${BASE_URL}/${INSTALL_FILE_REMOTE}"
  else
    echo "Found neither 'curl' nor 'wget'. Can't Continue."
    exit 1
  fi
fi

if [ ! -e "/tmp/${INSTALL_FILE}" ]
then
  echo "Error Downloading Install File"
  exit 1
fi


bash "/tmp/${INSTALL_FILE}" --no-dev-deps
